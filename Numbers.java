package ru.god.numbers;

/**
 * Класс, демонстрирующий преобразование различных чисел в двоичный код
 *
 * @author Горбачева, 16ИТ18к
 */
public class Numbers {
    public static void main(String[] args) {
        intToBites(389);
        intToBites(-386);
        doubleToBites(75.38);
        doubleToBites(Math.PI);
        chekingPrecision(5.0, 7, 30000);
    }

    /**
     * Метод, представляющий число типа int в двоичном коде
     *
     * @param number - число типа int
     */
    private static void intToBites(int number) {
        System.out.println("Разряды числа: " + Integer.toBinaryString(number));
    }

    /**
     * Метод, представляющий число типа double в двоичном коде
     *
     * @param number - число типа double
     */
    private static void doubleToBites(double number) {
        String sResult = Long.toBinaryString(Double.doubleToLongBits(number));
        System.out.println("Представление вещественного числа в формате чисел с плавающей точкой");
        System.out.format("Число: %5.2f\n", number);
        System.out.println("Формат чисел с плавающей точкой:");
        System.out.println(number > 0 ? "0" + sResult : sResult);
    }

    /**
     * Метод, демонстрирующий потерю точности числа при вычислениях
     *
     * @param value1 - делимое
     * @param value2 - делитель
     * @param value3 - второе слагаемое
     */
    private static void chekingPrecision(double value1, int value2, int value3) {
        double d = value1 / value2;
        System.out.format("Число: %10.16f\n", d);
        d += value3;
        System.out.format("Число: %10.16f\n", d);
    }
}
